class User
  attr_reader :pos
  attr_reader :id, :dead

  def initialize(user_id)
    @id = user_id
    @pos = {x: 0, y: 0}
    @dead = false
    @update = false
  end

  def change_pos(x, y)
    @pos[:x] = x
    @pos[:y] = y
    @update = true
  end

  def is_updated
    die unless @update
    @update = false
  end
  
  def die
   @dead = true
  end
end
