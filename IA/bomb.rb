class Bomb
  attr_reader :type, :intensity, :pos

  def initialize(pos, intensity, type)
    @intensity = intensity
    @type = type
    @pos = pos
  end

  def v_explose(map, bombs)
    up = false
    down = false
    x = @pos[:x]
    y = @pos[:y]
    (@intensity).times do |i|
      unless up
        up = true if map[y + i][x] == CONFIG['wall']
        if map[y + i][x] == CONFIG['block']
          map[y + i][x] = '0'
          up = true
        end
        bombs.each {|b| b.destroy(map, bombs) if b.pos == {x: x, y: y + i}}
      end
      unless down
        up = true if map[y - i][x] == CONFIG['wall']
        if map[y - i][x] == CONFIG['block']
          map[y - i][x] = '0'
          up = true
        end
        bombs.each {|b| b.destroy(map, bombs) if b.pos == {x: x, y: y - i}}
      end
    end
  end

  def h_explose(map, bombs)
    right = false
    left = false
    x = @pos[:x]
    y = @pos[:y]
    (@intensity).times do |i|
      unless right
        left = true if map[y][x + i] == CONFIG['wall']
        if map[y][x + i] == CONFIG['block']
          map[y][x + i] = '0'
          right = true
        end
        bombs.each {|b| b.destroy(map, bombs) if b.pos == {x: x + i, y: y}}
      end
      unless left
        left = true if map[y][x - i] == CONFIG['wall']
        if map[y][x - i] == CONFIG['block']
          map[y][x - i] = '0'
          left = true
        end
        bombs.each {|b| b.destroy(map, bombs) if b.pos == {x: x - i, y: y}}
      end
    end
  end

  def destroy(map, bombs)
    Thread::abort_on_exception = true
    sleep 0.7
    bombs.delete(self)
    v_explose(map, bombs)
    h_explose(map, bombs)
    sleep(0.5)
    map[@pos[:y]][@pos[:x]] = '0'
  end
end
