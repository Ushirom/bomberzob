class Fixnum
  def way
    return '1 ' if self == 10
    return '0 ' if self == 0
    return '2 ' if self == -10
    return '8 ' if self == 1
    return '4 ' if self == -1
    ''
  end
end
