#!/usr/bin/env ruby

require 'yaml'
require './IA/ia.rb'

CONFIG = YAML.load_file('./IA/config.yml')
def main
  write_instru = File.open("#{ARGV[0]}", 'w')
  read_instru = File.open("#{ARGV[1]}", 'r')
  map = []
  File.open(ARGV[2]) do |f|
    text = f.read
    file = text.split(' ')
    val	= file[0].to_i
    file[1].to_i.times {|i| map << file.last[((val * i)...(val * (i + 1)))]}
  end
  ia = IA.new(write_instru, read_instru, map, ARGV[3].to_i);
  begin ia.play
  rescue 
  end
end

main
