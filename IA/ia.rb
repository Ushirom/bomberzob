require './IA/user.rb'
require './IA/bomb.rb'
require './IA/overload.rb'

class IA
  def initialize(write_instru, read_instru, map, id = 0)
    @bombs_nb = 1
    @pos = {x: 0, y: 0}
    @user = []
    @id = id
    @bombs = []
    @map = map
    @write_instru = write_instru
    @read_instru = read_instru
    @start = false
    @finished = false
    @intensity = 2
    @safe = true
    @bomb_mutex = Mutex.new
  end

  def play
    threads = []
    Thread::abort_on_exception = true
    threads << Thread.new { retrieve_data }
    Thread::abort_on_exception = true
    threads << Thread.new { check_bombs }
    sleep 0.1 until is_launched?
    until is_finished?
      find_safer_place until is_safe?
      find_player_to_kill
    end
    threads.each(&:join)
  end

  private

  def find_safer_place
    done = false
    x = @pos[:x] - 1
    y = @pos[:y] - 1
    3.times do |j|
      3.times do |i|
        if is_free?(x + i, y + j) && safe_test({x: x + i, y: y}, true) != false && \
            safe_test({x: x, y: y + j}, true) != false
          next if i == 1 && j == 1
          @write_instru.puts(((j - 1) * 10).way + (i - 1).way)
          @write_instru.flush
          done = true
          sleep 0.7
          break
        end
      end
    end
    return if done
    i = rand(2) - 1
    i = i == 0 ? 1 : -1
    j = rand(2) - 1
    j = j == 0 ? 1 : -1
    @write_instru.puts((j * 10).way + i.way)
    @write_instru.flush
    sleep 0.7
  end

  def find_player_to_kill
    index = 800
    i = 0
    stock = 1000000000
    @user.each do |u|
      if (@pos[:x] - u.pos[:x]).abs + (@pos[:y] - u.pos[:y]).abs < stock && !u.dead
        index = i
        stock = (@pos[:x] - u.pos[:x]).abs + (@pos[:y] - u.pos[:y]).abs
      end
      i += 1
    end
    u = @user[index]
    if !u.nil?
      bomb = Bomb.new(@pos, @intensity, 'normal')
      drop_bomb if (!vertical_explosion(bomb, u.pos) || !horizontal_explosion(bomb, u.pos)) && can_escape?
      follow_player(u.pos)
    else
      @write_instru.puts('0 0 ')
    end
  end

  def follow_player(ppos)
    hor = 0
    ver = 0

    if ppos[:y] > @pos[:y]
      ver = 1
    elsif ppos[:y] < @pos[:y]
      ver = -1
    end

    if ppos[:x] > @pos[:x]
      hor = 1
    elsif ppos[:x] < @pos[:x]
      hor = -1
    end

    x = @pos[:x] + hor + hor * 0.3
    y = @pos[:y] + ver + ver * 0.3
    drop_bomb if !is_free?(@pos[:x], y) && is_destroyable?(@pos[:x], y) && can_escape?
    drop_bomb if !is_free?(x, @pos[:y]) && is_destroyable?(x, @pos[:y]) && can_escape?
    return unless @safe
    if safe_test({x: @pos[:x], y: y}, true) != false && \
        safe_test({x: x, y: @pos[:y]}, true) != false && \
        safe_test({x: x, y: y}, true) != false
      @write_instru.puts((ver * 10).way + hor.way)
    else
      @write_instru.puts('0 0 ')
    end
    @write_instru.flush
    sleep 0.1
  end

  def safe_test(pos, val = false)
    return 1 if !is_free?(pos[:x], pos[:y]) && val == true
    return false unless is_free?(pos[:x], pos[:y])
    mutex = Mutex.new
    @bombs.each do |bomb|
      threads = []
      safe = []
      Thread::abort_on_exception = true
      threads << Thread.new {mutex.synchronize {safe[0] = vertical_explosion(bomb, pos)}}
      threads << Thread.new {mutex.synchronize {safe[1] = horizontal_explosion(bomb, pos)}}
      threads.each(&:join)
      return false if safe.include?(false)
    end
    true
  end

  def retrieve_data
    until is_finished?
      Thread::abort_on_exception = true
      fds = IO.select([@read_instru], [], [])
      if fds[0][0] == @read_instru
        cmd = ''
        cmd = @read_instru.gets.split(' ') unless is_finished?
        next unless CONFIG['cmd'].has_key?(cmd[0])
        send(CONFIG['cmd'][cmd[0]], cmd)
      end
    end
  end

  def vertical_explosion(bomb, pos = nil)
    x = bomb.pos[:x]
    y = bomb.pos[:y] - bomb.intensity
    (bomb.intensity * 2 + 1).times do |i|
      next if y + i < 0
      return true  if has_wall?(x, y + i) && i > bomb.intensity
      return false if is_touching_player?(x, y + i, pos)
    end
    true
  end

  def horizontal_explosion(bomb, pos = nil)
    x = bomb.pos[:x] - bomb.intensity
    y = bomb.pos[:y]
    (bomb.intensity * 2 + 1).times do |i|
      next if x + i < 0
      return true  if has_wall?(x + i, y) && i > bomb.intensity
      return false if is_touching_player?(x + i, y, pos)
    end
    true
  end

  def find_bomb_by_pos(bpos)
    @bombs.each {|b| return b if b.pos == bpos}
    nil
  end

  def check_bombs
    mutex = Mutex.new
    until is_finished?
      s = true
      @bombs.each do |bomb|
        test = []
        threads = []
        Thread::abort_on_exception = true
        threads << Thread.new {mutex.synchronize {test[0] = vertical_explosion(bomb)}}
        threads << Thread.new {mutex.synchronize {test[1] = horizontal_explosion(bomb)}}
        threads.each(&:join)
        s = false if test.include?(false)
      end
      @safe = s
    end
  end

  #cmd: bomb x y intensity bomb_type
  def add_bombs(cmd)
    x = cmd[1].to_i
    y = cmd[2].to_i
    @map[y][x] = CONFIG['bomb']
    @bombs << Bomb.new({x: x, y: y}, cmd[3].to_i, cmd[4])
  end

  #cmd: player x y user_id state
  def change_player_pos(cmd)
    if cmd[1] == "done"
      @user.each(&:is_updated)
      return
    end
    user_id = cmd[3].to_i
    if user_id == @id
      @pos[:x] = cmd[1].to_f
      @pos[:y] = cmd[2].to_f
      return
    end
    @user << User.new(user_id) unless player_exists?(user_id)
    user = get_user_by_id(user_id)
    user.change_pos(cmd[1].to_f, cmd[2].to_f)
  end

  #cmd: destroy x y
  def clean_at(cmd)
    bpos = {x: cmd[1].to_i, y: cmd[2].to_i}
    if @map[bpos[:y]][bpos[:x]] == CONFIG['bomb']
      Thread::abort_on_exception = true
      Thread.new do
        bomb = find_bomb_by_pos(bpos) 
        @bomb_mutex.synchronize {bomb.destroy(@map, @bombs) unless bomb.nil?}
      end
    end
  end

  #cmd: finish
  def finish_game(cmd)
    @finished = true
  end

  #cmd: intensity num
  def change_intensity(cmd)
    @intensity = cmd[1].to_i
  end

  #cmd: start
  def start_game(cmd)
    @start = true
  end

  def is_launched?
    @start
  end

  def is_finished?
    @finished
  end

  def is_touching_player?(x, y, pos)
    return pos[:x].to_i == x.to_i && pos[:y].to_i == y.to_i unless pos.nil?
    @pos[:x].to_i == x.to_i && @pos[:y].to_i == y.to_i
  end

  def player_exists?(id)
    @user.each {|u| return true if u.id == id }
    false
  end

  def get_user_by_id(id)
    @user.each {|u| return u if u.id == id}
    nil
  end

  def can_escape?
    if safe_test({x: @pos[:x] + 1 , y: @pos[:y]})
      return true if safe_test({x: @pos[:x], y: @pos[:y] + 1}) || safe_test({x: @pos[:x], y: @pos[:y] - 1})
    end
    if safe_test({x: @pos[:x] - 1 , y: @pos[:y]})
      return true if safe_test({x: @pos[:x], y: @pos[:y] + 1}) || safe_test({x: @pos[:x], y: @pos[:y] - 1})
    end
    false
  end

  def is_safe?
    @safe
  end

  def has_wall?(x, y)
    return true if y >= @map.count
    @map[y.to_i][x.to_i] == CONFIG['wall'] || @map[y][x] == CONFIG['block']
  end

  def is_free?(x, y)
    @map[y.to_i][x.to_i] == CONFIG['none']
  end

  def is_destroyable?(x, y)
    @map[y.to_i][x.to_i] == CONFIG['block']
  end

  #movement
  def drop_bomb
    @write_instru.puts('16')
    @write_instru.flush
    self
  end
end
